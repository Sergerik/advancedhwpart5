create table flowers
(
    id     serial primary key,
    flower varchar(30),
    price  int
);

insert into flowers (flower, price)
values ('Розы', 100);
insert into flowers (flower, price)
values ('Лилии', 50);
insert into flowers (flower, price)
values ('Ромашки', 25);

select *
from flowers;

create table buyers
(
    id           serial primary key,
    first_name   varchar,
    last_name    varchar,
    phone_number varchar
);

insert into buyers (first_name, last_name, phone_number)
values ('Федор', 'Федюнин', '89044014985');
insert into buyers (first_name, last_name, phone_number)
values ('Алексей', 'Соловьев', '89514014985');
insert into buyers (first_name, last_name, phone_number)
values ('Мария', 'Тё', '89994014985');

select *
from buyers;

create table ordering
(
    id             serial primary key,
    clients_id     integer REFERENCES buyers,
    flowers_id     integer REFERENCES flowers,
    date_added     timestamp,
    flowers_amount int not null
        constraint che_flowers_amount_clients_ordering
            check ( flowers_amount > 0 and flowers_amount < 1001)

);

select * from ordering;

insert into ordering(id, clients_id, flowers_id, date_added, flowers_amount)
values (1, 1, 3, now() - interval '65d', 20);
insert into ordering(id, clients_id, flowers_id, date_added, flowers_amount)
values (2, 2, 2, now() - interval '43d', 200);
insert into ordering(id, clients_id, flowers_id, date_added, flowers_amount)
values (3, 3, 3, now() - interval '621d', 300);

select*
from ordering o
         join buyers c on c.id = o.clients_id
where o.id = 1;


select *
from ordering o
where o.clients_id = 1
  and date_added >= now() - interval '1month';



SELECT o.flowers_id, max(o.flowers_amount) as MAX_FLOWERS_AMOUNT
from ordering o
         join flowers f on f.id = o.flowers_id
group by o.flowers_id
order by MAX_FLOWERS_AMOUNT desc
limit 1;




